class TweetsController < ApplicationController
  before_action :set_tweet, only: [:show, :edit, :update, :destroy, :like]

  # GET /tweets
  # GET /tweets.json
  def index
    @tweets = Tweet.all().sort_by{ |h| h.created_at }.reverse
  end

  # GET /tweets/1
  # GET /tweets/1.json
  def show
  end

  # GET /tweets/new
  def new
    @tweet = Tweet.new
  end

  # GET /tweets/1/edit
  def edit
  end

  # POST /tweets
  # POST /tweets.json
  def create
    @tweet = Tweet.new(tweet_params)
    respond_to do |format|
      if @tweet.save
        format.html { redirect_to tweets_url, notice: 'Tweet was successfully created.' }
        format.json { render :show, status: :created, location: @tweet }
      else
        format.html { redirect_to tweets_url, alert: @tweet.errors.full_messages  }
        format.json { render json: @tweet.errors, status: :unprocessable_entity }
      end
    end
    (session[:current_user_id] ||= []) << Array(:current_user_id) unless session.include?(:current_user_id)
    session[:current_user_id].push(@tweet.id)
  end

  # PATCH/PUT /tweets/1
  # PATCH/PUT /tweets/1.json
  def update
    respond_to do |format|
      if @tweet.update(tweet_params)
        format.html { redirect_to @tweet, notice: 'Tweet was successfully updated.' }
        format.json { render :show, status: :ok, location: @tweet }
      else
        format.html { render :edit }
        format.json { render json: @tweet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tweets/1
  # DELETE /tweets/1.json
  def destroy
    if session[:current_user_id].include?(@tweet.id)
      @tweet.destroy
      session[:current_user_id].delete(@tweet.id)
      respond_to do |format|
        format.html { redirect_to tweets_url, notice: 'Tweet was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to tweets_url, alert: 'You are not allowed to delete this tweet' }
        format.json { head :Forbidden }
      end
    end
  end
  
  # PUT /tweet/1/like
  def like
    @tweet.likes += 1
    @tweet.save
    respond_to do |format|
        format.html { redirect_to tweets_url }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tweet
      @tweet = Tweet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tweet_params
      params.require(:tweet).permit(:author, :content)
    end
end
